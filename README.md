### workshop

### install
brew install kind

### create cluster
kind create cluster --image=kindest/node:v1.20.7@sha256:cbeaf907fc78ac97ce7b625e4bf0de16e3ea725daf6b04f930bd14c67c671ff9

### get cluster
export KUBECONFIG=$(kind get kubeconfig)

### delete cluster
kind delete cluster

### install argo cd
1. create argocd namespace `kubectl create namespace argocd` 
2. install argocd `kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/v2.1.7/manifests/install.yaml`
3. get argocd admin password `kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d`
4. login to argocd `argocd login <ARGOCD_SERVER>`
5. change password `argocd account update-password`

### how to deploy application
1. create application `argocd app create sample --repo https://gitlab.com/warapong.pj/workshop.git --revision develop --path base/sample --dest-server https://kubernetes.default.svc --dest-namespace default`
2. get application on argocd `argocd app get sample`
3. sync manifest file `argocd app sync sample`
4. check history `argocd app history sample`
5. rollback `argocd app rollback sample 1`
4. delete application `argocd app delete sample`

### install argo rollouts
1. create argo-rollouts namespace `kubectl create namespace argo-rollouts`
2. install argo rollouts `kubectl apply -n argo-rollouts -f https://github.com/argoproj/argo-rollouts/releases/download/v1.1.1/install.yaml`
3. install kubectl plugin [link](https://argoproj.github.io/argo-rollouts/installation/#kubectl-plugin-installation) 

### apply canary to deployment
1. create application `argocd app create canary --repo https://gitlab.com/warapong.pj/workshop.git --revision develop --path base/canary --dest-server https://kubernetes.default.svc --dest-namespace default`
2. sync manifest file `argocd app sync canary`
3. revise image version in Rollout kind and commit to git server
4. get new Rollout version `kubectl argo rollouts get rollout canary`
5. promote new version `kubectl argo rollouts promote canary`
6. delete application `argocd app delete canary`

### apply bluegreen to deployment
1. create application `argocd app create bluegreen --repo https://gitlab.com/warapong.pj/workshop.git --revision develop --path base/bluegreen --dest-server https://kubernetes.default.svc --dest-namespace default`
2. sync manifest file `argocd app sync bluegreen`
3. revise image version in Rollout kind and commit to git server
4. get new Rollout version `kubectl argo rollouts get rollout bluegreen`
5. promote new version `kubectl argo rollouts promote bluegreen`
6. delete application `argocd app delete bluegreen`

### install kubeseal
1. install kubeseal controller `kubectl apply -f https://github.com/bitnami-labs/sealed-secrets/releases/download/v0.16.0/controller.yaml`
2. install kubeseal cli [link](https://github.com/bitnami-labs/sealed-secrets#homebrew)

### seal secret
1. get cert from kubeseal controller `kubeseal --controller-name=sealed-secrets-controller --controller-namespace=kube-system --fetch-cert > cert.pem`
2. create secret file `kubectl create secret generic credential --dry-run=client --from-literal=USERNAME=root --from-literal=PASSWORD=toor -o yaml > secret.yaml`
3. seal secret `cat secret.yaml | kubeseal --controller-name=sealed-secrets-controller --controller-namespace=kube-system --cert=cert.pem --format=yaml > sealed-secret.yaml`
4. deploy seal secret `kubectl apply -f sealed-secret.yaml`

### reference
1. [argocd](https://argo-cd.readthedocs.io/en/stable/)
2. [argo rollouts](https://argoproj.github.io/argo-rollouts/)
3. [gitops concept](https://www.somkiat.cc/gitops-flow/)
4. [deployment strategy](https://thenewstack.io/deployment-strategies/)
5. [kubeseal](https://github.com/bitnami-labs/sealed-secrets)